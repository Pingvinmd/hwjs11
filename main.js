const form = document.querySelector(".password-form");
const spanMsg = form.querySelector("#msg");

form.addEventListener("click", (e) => {
  const iconTarget = e.target.closest("i");

  if (iconTarget) {
    iconTarget.classList.toggle("fa-eye");
    iconTarget.classList.toggle("fa-eye-slash");

    const type = iconTarget.classList.contains("fa-eye-slash")
      ? "text"
      : "password";
    iconTarget.previousElementSibling.setAttribute("type", type);
  }
});


document.getElementById('check').onclick = function(){
  let password1 = document.getElementById('password1').value;
  let password2 = document.getElementById('password2').value;

    if (password1 == password2)
  alert('You are welcome');
  else if (password1 !== password2)
  alert("Потрібно ввести однакові значення")
  
}


form.addEventListener(
  "focus",
  (e) => {
    const target = e.target.closest("input");
    if (target) {
      spanMsg.textContent = "";
    }
  },
  { capture: true }
);